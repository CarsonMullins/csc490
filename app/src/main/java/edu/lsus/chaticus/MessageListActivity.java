package edu.lsus.chaticus;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class MessageListActivity extends AppCompatActivity {

    private TextView nameView;
    private EditText editText;
    private Button sendBtn;
    private Conversation currentConversation;
    private RecyclerView messagesList;
    private MessagesAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);
        nameView = findViewById(R.id.nameView);
        editText = findViewById(R.id.editText);
        sendBtn = findViewById(R.id.sendBtn);


        currentConversation = (Conversation)getIntent().getSerializableExtra("Conversation");
        nameView.setText(currentConversation.getName());
        messagesList = findViewById(R.id.messagesList);
        messagesList.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MessagesAdapter(currentConversation, this);
        messagesList.setAdapter(adapter);

        sendBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String message = editText.getText().toString();
                currentConversation.addMessage(message);
                adapter.notifyDataSetChanged();
                messagesList.scrollToPosition(adapter.getItemCount()-1);
            }
        });

    }
}
