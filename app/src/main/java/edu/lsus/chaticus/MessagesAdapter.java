package edu.lsus.chaticus;
import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class MessagesAdapter extends RecyclerView.Adapter<MessagesAdapter.MyViewHolder> {

    private Context mContext;
    private Conversation conversation;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nameView;
        public TextView messageView;

        public MyViewHolder(View view) {
            super(view);
            nameView = view.findViewById(R.id.nameView);
            messageView = view.findViewById(R.id.messageView);
        }
    }


    public MessagesAdapter(Conversation conversation, Context mContext) {
        this.mContext = mContext;
        this.conversation = conversation;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.message_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        String name = conversation.getName();
        String message = conversation.getMessages().get(position);
        holder.messageView.setText(message);
        holder.nameView.setText(name);
    }

    @Override
    public int getItemCount() {
        return conversation.getMessages().size();
    }

}