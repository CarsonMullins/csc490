package edu.lsus.chaticus;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class ConversationsAdapter extends RecyclerView.Adapter<ConversationsAdapter.MyViewHolder> {

    private List<Conversation> conversationList;
    private Context mContext;

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView conversationName;
        public CardView conversationView;
        private RecyclerViewClickListener listener;

        public MyViewHolder(View view, RecyclerViewClickListener listener) {
            super(view);
            conversationName = view.findViewById(R.id.conversationName);
            conversationView = view.findViewById(R.id.conversationView);
            this.listener = listener;
            conversationView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            listener.onClick(view, getAdapterPosition());
        }
    }


    public ConversationsAdapter(List<Conversation> conversationList, Context mContext) {
        this.conversationList = conversationList;
        this.mContext = mContext;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.conversation_item, parent, false);
        return new MyViewHolder(itemView, new RecyclerViewClickListener() {
            @Override
            public void onClick(View view, int position) {
                Intent intent = new Intent(mContext, MessageListActivity.class);
                intent.putExtra("Conversation", conversationList.get(position));
                mContext.startActivity(intent);
            }
        });
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        Conversation conversation = conversationList.get(position);
        holder.conversationName.setText(conversation.getName());
    }

    @Override
    public int getItemCount() {
        return conversationList.size();
    }









}

interface RecyclerViewClickListener {
    void onClick(View view, int position);
}