package edu.lsus.chaticus

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import android.widget.Toast
import java.security.AccessController.getContext


class MainActivity : AppCompatActivity() {

    var conversations : ArrayList<Conversation> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (!app.session.hasValidCredentials()) {
            app.login(this)
        }
        else {
            if(savedInstanceState == null) {
                // Populate conversations
                var thomas = Conversation("Thomas")
                var jeffrey = Conversation("Jeffrey")
                var tiffany = Conversation("Tiffany")
                var rodger = Conversation("Rodger")
                conversations.add(thomas)
                conversations.add(jeffrey)
                conversations.add(tiffany)
                conversations.add(rodger)
            }
            // Set up Conversations RecyclerView
            conversationList.layoutManager = LinearLayoutManager(this)
            conversationList.adapter = ConversationsAdapter(conversations, this)

            app.api.getMessages(1).enqueue(object : Callback<List<MessageDto>> {
                override fun onResponse(call: Call<List<MessageDto>>?, response: Response<List<MessageDto>>?) {

                }

                override fun onFailure(call: Call<List<MessageDto>>?, t: Throwable?) {

                }
            })

        }
    }
}