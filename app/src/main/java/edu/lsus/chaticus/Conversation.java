package edu.lsus.chaticus;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class Conversation implements Serializable {

    private String name;
    private ArrayList<String> messages;

    public Conversation(String name) {
        this.name = name;
        messages = new ArrayList<>();
    }

    public void addMessage(String message) {
        messages.add(message);
    }

    public ArrayList<String> getMessages() {
        return messages;
    }

    public void setName(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

}
